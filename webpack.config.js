const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: "./src/js/main.js",
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: "./app.bundle.js"
    },

    module: {
        rules: [{
            test: /\.less$/,
            use: [MiniCssExtractPlugin.loader, {
                loader: 'css-loader', options: {
                    sourceMap: true
                }
            }, {
                loader: 'less-loader', options: {
                    sourceMap: true
                }
            }],
        }, {
            test: /\.pug$/,
            use: ['pug-loader']
        }]
    },

    plugins: [
        new MiniCssExtractPlugin(),
        new HtmlWebpackPlugin({
            template: './src/pug/_index.pug'
        }),
    ]
}