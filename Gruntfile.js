module.exports = function (grunt) {
    grunt.initConfig({
        pug: {
            html: {
                files: [{
                    expand: true,
                    cwd: 'src/pug/',
                    src: ['./**/*.pug'],
                    dest: 'build/',
                    ext: '.html'
                }]
            }
        },

        less: {
            development: {
                options: {
                    cwd: 'src/less',
                    dumpLineNumbers: 'all',
                    relativeUrls: true,
                    sourceMap: true,
                    sourceMapFileInline: true
                },
                files: {
                    "build/style.css": "src/less/style.less"
                }
            },

            build: {
                options: {
                    cwd: 'src/less',
                    dumpLineNumbers: 'none',
                    sourceMap: false
                },
                files: {
                    "build/style.css": "src/less/style.less"
                }
            }
        },

        watch: {
            pug: {
                files: ['src/pug/*.pug'],
                tasks: ['pug']
            },
            less: {
                files: ['src/less/*.less'],
                tasks: ['less']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-pug');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', [ 'pug', 'less:development', 'watch' ]);
    grunt.registerTask('build', [ 'pug', 'less:build' ]);
};
